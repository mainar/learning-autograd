# Notes on how I proceeded:

* writing unit tests to check that my logic yielding the same results as coursera's implementation
* when implementing 1 FC layer I had a big headache
    * For fast iteration I fixed the following hyperparameters: hidden_units=10, epochs=2000, learning_rate=0.01
        * Best accuracies in test would range from 66% to 72%
    * my mistake first was initializing the weights and biases to zeros
        * this showed up as having the loss plateau very early, and hence having a very poor
        accuracy in both the training and the test set
        * this works on a logistic regression but
        * doesnt work on NN because of what is called the symetry problem. Essentially
        all your hidden units will end up calculating the same function (being symetric)
    * my attempt to fix it was to initialize Weights and Biases to random values `torch.rand`
        * this didnt work either
        * z1, A1, and z2 had values >> 1000
        * A2 = sigmoid(z2) was all ones
        * I was using Log Loss (Cross Entropy Loss) function and is was yielding NaN
        * hypothesis: are the weights too big?
    * initialize randomly but divide by a power of 10, e.g., `torch.rand() * 0.01`
        * I tried different values, 0.1, 0.01, 0.001 and 0.0001
        * Smaller than 0.1 would return a sensible value for the loss
        * But, for some reason, the grandients (`self.w1.grad`) return None
        * I can't come up with an explanation for this, so I'll move forward and try something else
    * instead of using my own initialization, I would use one of the available ones:
        ```
        self.w1 = torch.zeros((X.shape[0], self.hidden_units), requires_grad=True, dtype=torch.float64)
        torch.nn.init.normal_(self.w1, mean=0, std=0.01)
        ```
        * Default values (mean=0, std=1) had the NaN problem
        * Playing with values I foudn that mean=0, std=0.01 fixed it
        * mean=0, std=0.1 still gave NaN
        * mean=0, std=0.01 yielded the best results
    * other intializations: Xavier Normal and Xavier Uniform
        ```
        torch.nn.init.xavier_normal_(self.w1)
        torch.nn.init.xavier_normal_(self.w1, gain=torch.nn.init.calculate_gain('relu'))
        torch.nn.init.xavier_uniform_(self.w1)
        torch.nn.init.xavier_uniform_(self.w1, gain=torch.nn.init.calculate_gain('relu'))
        ```
        * both Normal and Uniform showed the same behaviour
        * worked out of the box with default params
        * if gain is not calculated: 66% accuracy test
        * of gain is calculated: 72% accuracy test
    * Best Scores achieved:
        * Heavy loading
        train accuracy: 100.0
        test accuracy: 76.0
            * xavier_normal_ with gain
            * hidden_units = 1000
            * epochs=10000
            * learning_rate=0.01
            * ~ 40 minutes training
        * Lightweight
        This config is very fast to train (~30 seconds) but fluctuates in performance depending (I guess)
        on the initial randomization. Results go from 70% accuracy on test upto 80%, many times being 72% or 76%
            * xavier_normal_ with gain
            * hidden_units = 10
            * epochs=1000
            * learning_rate=0.01
            * ~ 30 seconds
# Ideas for next steps

* Implement my own ReLU and Sigmoid functions with Autograd
    https://github.com/jcjohnson/pytorch-examples#pytorch-defining-new-autograd-functions

* Generalising nn1hl to accept L hidden layers

* Extracting the logic of the optimizer away from the model. Get inspired by torch.Optimizer
    https://pytorch.org/docs/stable/optim.html
