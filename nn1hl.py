"""
My attempt at reproducing Coursera's logistic regresion example with autograd
"""
import numpy as np
import torch

class NN1HiddenLayerModel():
    def __init__(self):
        self.w1 = None
        self.b1 = None
        self.w2 = None
        self.b2 = None
        self.hidden_units = 10

    def train(self, X, Y, epochs=1000, learning_rate=0.5):
        self.w1 = torch.zeros((X.shape[0], self.hidden_units), requires_grad=True, dtype=torch.float64)
        self.b1 = torch.zeros((self.hidden_units, 1)         , requires_grad=True, dtype=torch.double)
        self.w2 = torch.zeros((self.hidden_units, 1)         , requires_grad=True, dtype=torch.float64)
        self.b2 = torch.zeros((1,1)                          , requires_grad=True, dtype=torch.double)

        # torch.nn.init.normal_(self.w1, mean=0, std=0.01)
        # torch.nn.init.normal_(self.b1, mean=0, std=0.01)
        # torch.nn.init.normal_(self.w2, mean=0, std=0.01)
        # torch.nn.init.normal_(self.b2, mean=0, std=0.01)

        torch.nn.init.xavier_normal_(self.w1, gain=torch.nn.init.calculate_gain('relu'))
        torch.nn.init.xavier_normal_(self.b1, gain=torch.nn.init.calculate_gain('relu'))
        torch.nn.init.xavier_normal_(self.w2, gain=torch.nn.init.calculate_gain('sigmoid'))
        torch.nn.init.xavier_normal_(self.b2, gain=torch.nn.init.calculate_gain('sigmoid'))

        # torch.nn.init.xavier_uniform_(self.w1, gain=torch.nn.init.calculate_gain('relu'))
        # torch.nn.init.xavier_uniform_(self.b1, gain=torch.nn.init.calculate_gain('relu'))
        # torch.nn.init.xavier_uniform_(self.w2, gain=torch.nn.init.calculate_gain('sigmoid'))
        # torch.nn.init.xavier_uniform_(self.b2, gain=torch.nn.init.calculate_gain('sigmoid'))


        m = X.shape[1]
        for i in range(epochs):
            z1 = self.w1.transpose(0, 1).mm(X).add(self.b1)
            A1 = z1.clamp(min=0) # ReLu
            z2 = self.w2.transpose(0, 1).mm(A1).add(self.b2)
            A2 = torch.sigmoid(z2)

            loss = Y.mul(A2.log()) + (1-Y).mul((1-A2).log())
            loss = loss.sum()/(-m)
            loss.backward()
            # import pdb; pdb.set_trace()

            with torch.no_grad():
                self.w1 -= learning_rate * self.w1.grad
                self.b1 -= learning_rate * self.b1.grad
                self.w2 -= learning_rate * self.w2.grad
                self.b2 -= learning_rate * self.b2.grad
                # Manually zero the gradients after running the backward pass
                self.w1.grad.zero_()
                self.b1.grad.zero_()
                self.w2.grad.zero_()
                self.b2.grad.zero_()
            if i % 100 == 0:
                # import pdb; pdb.set_trace()
                print ("Loss after iteration %i: %f" %(i, loss))
    def predict(self, X):
        with torch.no_grad():
            z1 = self.w1.transpose(0, 1).mm(X).add(self.b1)
            A1 = z1.clamp(min=0) # ReLu
            z2 = self.w2.transpose(0, 1).mm(A1).add(self.b2)
            Y_pred = torch.sigmoid(z2)

            Y_pred[Y_pred<0.5] = 0
            Y_pred[Y_pred>=0.5] = 1
            return Y_pred

    def benchmark(self, X, Y):
        Y_pred = self.predict(X)
        accuracy = np.mean(np.abs(Y_pred.numpy() - Y.numpy()))
        accuracy = 100 - 100 * accuracy
        return accuracy


if __name__ == "__main__":
    from coursera01w02.lr_utils import load_dataset

    train_set_x_orig, train_set_y, test_set_x_orig, test_set_y, classes = load_dataset()
    train_set_x_flatten = train_set_x_orig.reshape(train_set_x_orig.shape[0],-1).T
    test_set_x_flatten = test_set_x_orig.reshape(test_set_x_orig.shape[0],-1).T
    train_set_x = train_set_x_flatten/255.
    test_set_x = test_set_x_flatten/255.

    train_set_x = torch.from_numpy(train_set_x).type(torch.float64)
    train_set_y = torch.from_numpy(train_set_y).type(torch.float64)
    test_set_x = torch.from_numpy(test_set_x).type(torch.float64)
    test_set_y = torch.from_numpy(test_set_y).type(torch.float64)
    # d = model(train_set_x, train_set_y, test_set_x, test_set_y, num_iterations = 2000, learning_rate = 0.005, print_cost = True)
    nn1hl = NN1HiddenLayerModel()
    nn1hl.train(train_set_x, train_set_y, epochs=1000, learning_rate=0.01)
    train_accuracy = nn1hl.benchmark(train_set_x, train_set_y)
    test_accuracy = nn1hl.benchmark(test_set_x, test_set_y)
    print(f"train accuracy: {train_accuracy}")
    print(f"test accuracy: {test_accuracy}")